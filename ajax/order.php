<?
use PHPMailer\PHPMailer\PHPMailer;

require 'phpmailer/Exception.php';
require 'phpmailer/OAuth.php';
require 'phpmailer/PHPMailer.php';
require 'phpmailer/POP3.php';
require 'phpmailer/SMTP.php';
$mail = new PHPMailer;
$mail->setFrom("referral@svoe.io");
$mail->addAddress('oficalpartner@yandex.ru');
$mail->CharSet = "UTF-8";
$mail->Subject = htmlspecialchars('Заявка на регистрацию на сайте referral.svoe.io');
$message = '';
$message = '
		<html>
			<head>
				<title>'.$subject.'</title>
			</head>
			<body>
				<p>Имя: '.$_POST['name'].'</p>
				<p>Телефон: '.$_POST['phone'].'</p>                        
			</body>
		</html>'; 
$mail->msgHTML($message);
if (!$mail->send()) {
	echo json_encode(['success' => false, 'msg' => "Mailer Error: " . $mail->ErrorInfo]);
} else {
	$message = '<div class="thanks">
					<div class="thanks-top">СПАСИБО ЗА ЗАЯВКУ!</div>
					<div class="thanks-text">Наш менеджер свяжется с Вами в ближайшее время</div>
				</div>';
	echo json_encode(['success' => true, 'msg' => $message]);
}
?>
